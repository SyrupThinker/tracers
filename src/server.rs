use super::*;

#[derive(Clone)]
pub struct Server {
    client: reqwest::Client,
    address: String,
}

impl Server {
    pub fn new(address: String) -> Result<Server, Error> {
        let client = reqwest::Client::new()?;

        let resp = client.get(&format!("{}/_matrix/client/versions", &address))?.send()?;

        let json: json::Value = json::from_reader(resp)?;
        if !json["versions"].as_array().unwrap_or(&Vec::new()).iter().any(|x| x == "r0.3.0") {
            return Err(Error {
                kind: ErrorKind::ApiVersion
            })
        } else {
            Ok(Server {
                client,
                address
            })
        }
    }

    pub fn address(&self) -> &str {
        &self.address
    }

    pub fn login(&self, user: &str, password: &str, device_name: &str) -> Result<(Tokens, resp::LoginResponse), Error> {
        // I don't know where initial_device_display_name is documented, but thanks Riot, this makes my life easier
        
        let client = &self.client;
        
        let res = client
            .post(&format!("{}/_matrix/client/r0/login", &self.address))?
        .json(&json!({
            "type": "m.login.password",
            "user": user,
            "password": password,
            "initial_device_display_name": device_name,
        }))?
        .send()?;
        
        let json = json::from_reader(res)?;
        let json = err_check(json)?;

        let login: resp::LoginResponse = json::from_value(json)?;
        let token = Tokens(login.access_token.clone(), login.refresh_token.clone());

        Ok((token, login))
    }

    pub fn tokenrefresh(&self, token: Tokens) -> Result<Tokens, Error> {
        let client = &self.client;

        if token.1.is_none() {
            Err(Error {
                kind: ErrorKind::NoRefreshToken
            })
        } else {
            let res = client
                .post(&format!("{}/_matrix/client/r0/tokenrefresh", &self.address))?
            .json(&json!({
                "refresh_token": token.1.unwrap(),
            }))?
            .send()?;

            let json = json::from_reader(res)?;
            let json = err_check(json)?;

            let token: Tokens = json::from_value(json)?;

            Ok(token)
        }
    }

    pub fn get_room(&self, room: String) -> Result<room::Room, Error> {
        if room.starts_with('#') {
            self.resolve_alias(&room)
        } else if room.starts_with('!') {
            room::Room::new(self.address.clone(), room, Vec::new()) // Improve
        } else {
            Err(Error {
                kind: ErrorKind::InvalidRoomString,
            })
        }
    }

    pub fn resolve_alias(&self, alias: &str) -> Result<room::Room, Error> {
        let client = &self.client;

        let alias = alias.replace('#', "%23");
        let res = client
            .get(&format!(
                "{}/_matrix/client/r0/directory/room/{}",
                &self.address,
                alias
            ))?
        .send()?;

        let json = json::from_reader(res)?;
        let json = err_check(json)?;

        let room: resp::RoomResponse = json::from_value(json)?;

        Ok(room::Room::new(self.address.clone(), room.room_id, room.servers)?)
    }

    pub fn public_rooms(&self) -> Result<resp::PublicRoomsResponse, Error> {
        let client = &self.client;

        let resp = client.get(&format!("{}/_matrix/client/r0/publicRooms", &self.address))?.send()?;

        let json = json::from_reader(resp)?;
        let json = err_check(json)?;

        let rms: resp::PublicRoomsResponse = json::from_value(json)?;

        Ok(rms)
    }

    pub fn logout(&self, token: Tokens) -> Result<(), Error> {
        let client = &self.client;

        let _ = client
            .post(&format!(
                "{}/_matrix/client/r0/logout?access_token={}",
                &self.address,
                &token.0
            ))?
        .send()?;

        Ok(())
    }

    pub fn delete_device(&self, token: &Tokens, username: &str, password: &str, device_id: &str) -> Result<(), Error> {
        let client = &self.client;
        
        let json: json::Value = client.delete(&format!("{}/_matrix/client/unstable/devices/{}?access_token={}", self.address(), device_id, token.0))?.send()?.json()?;

        let json = client.delete(&format!("{}/_matrix/client/unstable/devices/{}?access_token={}", self.address(), device_id, token.0))?
        .body(json!({
            "auth": {
                "session": json["session"].as_str().unwrap_or(""),
                "type": "m.login.password",
                "user": username,
                "password": password,
            }
        }).to_string()).send()?.json()?;

        let _ = err_check(json)?;

        Ok(())
    }

    pub fn sync(&self, access_token: &Tokens, since: &str) -> Result<resp::SyncResponse, Error> {
        let client = &self.client;

        let res = client
            .get(&if since == "" {
                format!(
                    "{}/_matrix/client/r0/sync?access_token={}",
                    &self.address,
                    &access_token.0
                )
            } else {
                format!(
                    "{}/_matrix/client/r0/sync?access_token={}&since={}",
                    &self.address,
                    &access_token.0,
                    since
                )
            })?
            .send()?;

        let json = json::from_reader(res)?;
        let json = err_check(json)?;

        let sresp: resp::SyncResponse = json::from_value(json)?;
        
        Ok(sresp)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Tokens(pub String, pub Option<String>);
