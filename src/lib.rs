pub mod resp;
pub mod room;
pub mod events;
pub mod server;

extern crate reqwest;
#[macro_use]
extern crate serde_json as json;
#[macro_use]
extern crate serde_derive;

use std::fmt;
use std::error::Error as SError;
use std::convert::From;

#[derive(Debug)]
pub struct Error {
    pub kind: ErrorKind,
}

#[derive(Debug)]
pub enum ErrorKind {
    Request(reqwest::Error),
    Json(json::Error),
    ApiVersion,
    Api(ApiError),
    NoRefreshToken,
    InvalidRoomString,
}

#[derive(Debug, Deserialize)]
pub struct ApiError {
    pub errcode: String,
    pub error: String,
}

impl SError for ApiError {
    fn description(&self) -> &str {
        &self.error
    }

    fn cause(&self) -> Option<&SError> {
        None
    }
}

impl SError for Error {
    fn description(&self) -> &str {
        match self.kind {
            ErrorKind::Request(ref e) => e.description(),
            ErrorKind::Json(ref e) => e.description(),
            ErrorKind::ApiVersion => "No compatible API version found",
            ErrorKind::Api(ref e) => &e.error,
            ErrorKind::NoRefreshToken => "No refresh_token available",
            ErrorKind::InvalidRoomString => "Provided an invalid room string",
        }
    }

    fn cause(&self) -> Option<&SError> {
        match self.kind {
            ErrorKind::Request(ref e) => e.cause(),
            ErrorKind::Json(ref e) => e.cause(),
            _ => None,
        }
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Error {
            kind: ErrorKind::Request(error)
        }
    }
}

impl From<json::Error> for Error {
    fn from(error: json::Error) -> Self {
        Error {
            kind: ErrorKind::Json(error)
        }
    }
}

fn err_check(value: json::Value) -> Result<json::Value, Error> {
    if value["errcode"].is_string() {
        let apie: ApiError = json::from_value(value)?;
        return Err(Error{
            kind: ErrorKind::Api(apie)
        })
    }

    Ok(value)
}

