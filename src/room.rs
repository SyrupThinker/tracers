use super::*;

#[derive(Debug)]
pub struct Room {
    client: reqwest::Client,
    server_address: String,
    room_id: String,
    servers: Vec<String>,
}

pub enum Direction {
    Forward,
    Backward,
}

impl std::string::ToString for Direction {
    fn to_string(&self) -> String {
        match self {
            &Direction::Forward => "f".to_string(),
            &Direction::Backward => "b".to_string(),
        }
    }
}

impl Room {
    pub fn new(server_address: String, room_id: String, servers: Vec<String>) -> Result<Room, Error> {
        Ok(Room {
            client: reqwest::Client::new()?,
            server_address,
            room_id,
            servers,
        })
    }

    pub fn id(&self) -> &str {
        &self.room_id
    }

    pub fn servers(&self) -> &Vec<String> {
        &self.servers
    }

    pub fn join(&self, access_token: &server::Tokens) -> Result<(), Error> {
        let client = &self.client;

        let resp = client.post(&format!("{}/_matrix/client/r0/rooms/{}/join?access_token={}",
                                       &self.server_address, &self.room_id, &access_token.0))?
        .send()?;

        let json = json::from_reader(resp)?;
        let _ = err_check(json)?;

        Ok(())
    }

    pub fn leave(&self, access_token: &server::Tokens) -> Result<(), Error> {
        let client = &self.client;

        let resp = client.post(&format!("{}/_matrix/client/r0/rooms/{}/leave?access_token={}",
                                       &self.server_address, &self.room_id, &access_token.0))?
        .send()?;

        let json = json::from_reader(resp)?;
        let _ = err_check(json)?;

        Ok(())
    }
    
    pub fn messages(&self, access_token: &server::Tokens, from: &str, dir: Direction) -> Result<resp::MessagesResponse, Error> {
        let client = &self.client;

        let resp = client.get(&format!("{}/_matrix/client/r0/rooms/{}/messages?access_token={}",
                                       &self.server_address, &self.room_id, &access_token.0))?
        .json(&json!({
            "from": from,
            "dir": dir.to_string(),
        }))?.send()?;

        let json = json::from_reader(resp)?;
        let json = err_check(json)?;

        let mres: resp::MessagesResponse = json::from_value(json)?;
        
        Ok(mres)
    }

    pub fn state(&self, access_token: &server::Tokens) -> Result<Vec<events::Event>, Error> {
        let client = &self.client;

        let resp = client.get(&format!("{}/_matrix/client/r0/rooms/{}/state?access_token={}",
                                       &self.server_address, &self.room_id, &access_token.0))?
        .send()?;

        let json = json::from_reader(resp)?;
        let json = err_check(json)?;

        let sev: Vec<events::Event> = json::from_value(json)?;
        
        Ok(sev)
    }

    pub fn set_alias(&self, access_token: &server::Tokens, alias: &str) -> Result<(), Error> {
        let client = &self.client;

        let alias = alias.replace('#', "%23");

        let resp = client.put(&format!("{}/_matrix/client/r0/directory/room/{}?access_token={}",
                                       &self.server_address, alias, access_token.0))?
            .json(&json!({
                "room_id": &self.room_id,
            }))?.send()?;

        let json = json::from_reader(resp)?;
        let _ = err_check(json)?;

        Ok(())
    }

    pub fn redact_event(&self, token: &server::Tokens, event_id: &events::EventId, reason: &str) -> Result<events::EventId, Error> {
        let client = &self.client;

        let pson = if reason == "" {
            json!({})
        } else {
            json!({
                "reason": reason,
            })
        };
        
        let resp = client.put(&format!("{}/_matrix/client/r0/rooms/{}/redact/{}/{}?access_token={}",
                                       &self.server_address, &self.room_id, event_id, std::time::SystemTime::now()
                                       .duration_since(std::time::UNIX_EPOCH)
                                       .unwrap()
                                       .as_secs(),
                                       &token.0))?
        .json(&pson)?.send()?;

        let json = json::from_reader(resp)?;
        let json = err_check(json)?;

        let eid = events::EventId(json["event_id"].as_str().unwrap().to_string());

        Ok(eid)
    }

    pub fn send(
        &self,
        token: &server::Tokens,
        event_type: events::EventType,
        message: &json::Value,
    ) -> Result<events::EventId, Error> {
        let client = &self.client;

        let res = client
            .put(&format!(
                "{}/_matrix/client/r0/rooms/{}/send/{}/{}?access_token={}",
                &self.server_address,
                &self.room_id,
                event_type.0,
                std::time::SystemTime::now()
                    .duration_since(std::time::UNIX_EPOCH)
                    .unwrap()
                    .as_secs(),
                &token.0
            ))?
        .json(message)?
            .send()?;
        
        let json = json::from_reader(res)?;
        let json = err_check(json)?;

        let eid = events::EventId(json["event_id"].as_str().unwrap().to_string());

        Ok(eid)
    }

    pub fn send_state(
        &self,
        token: &server::Tokens,
        event_type: events::EventType,
        message: &json::Value,
        state_key: &str,
    ) -> Result<events::EventId, Error> {
        let client = &self.client;

        let res = client
            .put(&format!(
                "{}/_matrix/client/r0/rooms/{}/state/{}/{}?access_token={}",
                &self.server_address,
                &self.room_id,
                event_type.0,
                state_key,
                &token.0
            ))?
        .json(message)?
            .send()?;
        
        let json = json::from_reader(res)?;
        let json = err_check(json)?;
        
        let eid: events::EventId = json::from_value(json)?;

        Ok(eid)
    }
}
