use ::std::collections::HashMap;

type EventContent = ::json::Value;

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum Event { // We do this because there is no direct fallback for tag-enums
    Known(EventTyped),
    Unknown(::json::Value),
}

#[derive(Debug, Deserialize)]
#[serde(tag="type")]
pub enum EventTyped {
    #[serde(rename="m.room.message")]
    RoomMessage {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        content: MessageContent,
    },
    #[serde(rename="m.room.member")]
    RoomMember {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<MemberContent>,
        content: MemberContent,
        invite_room_state: Option<Vec<StrippedState>>,
    },
    #[serde(rename="m.room.avatar")]
    RoomAvatar {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<AvatarContent>,
        content: AvatarContent,
    },
    #[serde(rename="m.room.create")]
    RoomCreate {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<CreateContent>,
        content: CreateContent,
    },
    #[serde(rename="m.room.power_levels")]
    RoomPowerLevels {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<PowerLevelsContent>,
        content: PowerLevelsContent,
    },
    #[serde(rename="m.room.join_rules")]
    RoomJoinRules {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<JoinRulesContent>,
        content: JoinRulesContent,
    },
    #[serde(rename="m.room.history_visibility")]
    RoomHistoryVisibility {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<HistoryVisibilityContent>,
        content: HistoryVisibilityContent,
    },
    #[serde(rename="m.room.guest_access")]
    RoomGuestAccess {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<GuestAccessContent>,
        content: GuestAccessContent,
    },
    #[serde(rename="m.room.redaction")]
    RoomRedaction {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        redacts: EventId,
        content: RedactionContent,
    },
    #[serde(rename="m.room.third_party_invite")]
    RoomThirdPartyInvite {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Unsigned,
        state_key: StateKey,
        prev_content: Option<ThirdPartyInviteContent>,
        content: ThirdPartyInviteContent,
    },
    #[serde(rename="m.room.encrypted")]
    RoomEncrypted {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        content: EncryptedContent,
    },
    #[serde(rename="m.room.aliases")]
    RoomAliases {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<AliasesContent>,
        content: AliasesContent,
    },
    #[serde(rename="m.room.canonical_alias")]
    RoomCanonicalAlias {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<CanonicalAliasContent>,
        content: CanonicalAliasContent,
    },
    #[serde(rename="m.room.topic")]
    RoomTopic {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<TopicContent>,
        content: TopicContent,
    },
    #[serde(rename="m.room.name")]
    RoomName {
        origin_server_ts: f64,
        sender: UserId,
        event_id: EventId,
        room_id: Option<RoomId>,
        unsigned: Option<Unsigned>,
        state_key: StateKey,
        prev_content: Option<NameContent>,
        content: NameContent,
    },
}

#[derive(Debug, Deserialize)]
pub struct StrippedState {
    pub state_key: StateKey,
    pub type_: String,
    pub content: EventContent, // TODO Make Content an enum
}

#[derive(Debug, Deserialize)]
pub struct CanonicalAliasContent {
    pub alias: String,
}

#[derive(Debug, Deserialize)]
pub struct NameContent {
    pub name: String,
}

#[derive(Debug, Deserialize)]
pub struct TopicContent {
    pub topic: String,
}

#[derive(Debug, Deserialize)]
pub struct AliasesContent {
    pub aliases: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct RedactionContent {
    pub reason: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct EncryptedContent {
    pub algorithm: String,
    pub ciphertext: String,
    pub device_id: String,
    pub sender_key: String,
    pub session_id: String,
}

#[derive(Debug, Deserialize)]
pub struct GuestAccessContent {
    pub guest_access: GuestAccess,
}    

#[derive(Debug, Deserialize)]
pub enum GuestAccess {
    #[serde(rename="can_join")]
    CanJoin,
    #[serde(rename="forbidden")]
    Forbidden,
}

#[derive(Debug, Deserialize)]
pub enum HistoryVisibility {
    #[serde(rename="invited")]
    Invited,
    #[serde(rename="joined")]
    Joined,
    #[serde(rename="shared")]
    Shared,
    #[serde(rename="world_readable")]
    WorldReadable,
}

#[derive(Debug, Deserialize)]
pub struct HistoryVisibilityContent {
    history_visibility: HistoryVisibility,
}

#[derive(Debug, Deserialize)]
pub struct PowerLevelsContent {
    pub events_default: f64,
    pub invite: f64,
    pub state_default: f64,
    pub redact: f64,
    pub ban: f64,
    pub users_default: f64,
    pub events: HashMap<String, f64>,
    pub kick: f64,
    pub users: HashMap<UserId, f64>,
}

#[derive(Debug, Deserialize)]
pub enum JoinRule {
    #[serde(rename="public")]
    Public,
    #[serde(rename="knock")]
    Knock,
    #[serde(rename="private")]
    Private,
    #[serde(rename="invite")]
    Invite,
}

#[derive(Debug, Deserialize)]
pub struct JoinRulesContent {
    pub join_rule: JoinRule,
}

#[derive(Debug, Deserialize)]
pub struct CreateContent {
    pub creator: UserId,
    #[serde(rename="m.federate")]
    #[serde(default="true_f")]
    pub m_federate: bool,
}

#[derive(Debug, Deserialize)]
pub struct AvatarContent {
    pub url: String,
}

fn true_f() -> bool {
    true
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum MessageContent {
    Known(MessageContentTyped),
    Unknown(::json::Value),
}

#[derive(Debug, Deserialize)]
#[serde(tag="msgtype")]
pub enum MessageContentTyped {
    #[serde(rename="m.text")]
    Text {
        body: String,
    },
    #[serde(rename="m.emote")]
    Emote {
        body: String,
    },
    #[serde(rename="m.notice")]
    Notice {
        body: String,
    },
    #[serde(rename="m.image")]
    Image {
        body: String,
        url: String,
        info: Option<ImageInfo>,
    },
    #[serde(rename="m.file")]
    File {
        body: String,
        filename: String,
        url: String,
        info: Option<FileInfo>,
    },
    #[serde(rename="m.location")]
    Location {
        body: String,
        geo_uri: String,
        info: Option<LocationInfo>,
    },
    #[serde(rename="m.video")]
    Video {
        body: String,
        url: String,
        info: Option<VideoInfo>,
    },
    #[serde(rename="m.audio")]
    Audio {
        body: String,
        url: String,
        info: Option<AudioInfo>,
    },
}

#[derive(Debug, Deserialize)]
pub struct AudioInfo {
    pub duration: f64,
    pub mimetype: String,
    pub size: f64,
}

#[derive(Debug, Deserialize)]
pub struct VideoInfo {
    pub duration: f64,
    pub h: f64,
    pub w: f64,
    pub mimetype: String,
    pub size: f64,
    pub thumbnail_url: String,
    pub thumbnail_info: ThumbnailInfo,
}

#[derive(Debug, Deserialize)]
pub struct LocationInfo {
    pub thumbnail_url: String,
    pub thumbnail_info: ThumbnailInfo,
}

#[derive(Debug, Deserialize)]
pub struct FileInfo {
    pub mimetype: String,
    pub size: f64,
    pub thumbnail_url: String,
    pub thumbnail_info: ThumbnailInfo,
}

#[derive(Debug, Deserialize)]
pub struct ImageInfo {
    pub h: f64,
    pub w: f64,
    pub mimetype: String,
    pub size: f64,
    pub thumbnail_url: String,
    pub thumbnail_info: ThumbnailInfo,
}

#[derive(Debug, Deserialize)]
pub struct ThumbnailInfo {
    pub h: f64,
    pub w: f64,
    pub mimetype: String,
    pub size: f64,
}

#[derive(Debug, Deserialize)]
pub struct MemberContent {
    pub membership: Membership,
    pub avatar_url: Option<String>,
    pub displayname: Option<String>,
    pub is_direct: Option<bool>,
    pub third_party_invite: Option<ThirdPartyInvite>,
}

#[derive(Debug, Deserialize)]
pub struct ThirdPartyInvite {
    pub display_name: String,
    pub signed: Signed, // Clients should ignore this, maybe we should remove it
}

#[derive(Debug, Deserialize)]
pub struct PublicKeys {
    pub key_validity_url: String,
    pub public_key: String,
}

#[derive(Debug, Deserialize)]
pub struct ThirdPartyInviteContent {
    pub display_name: String,
    pub key_validity_url: String,
    pub public_key: String,
    pub public_keys: Vec<PublicKeys>,
}

#[derive(Debug, Deserialize)]
pub struct Signed {
    pub mxid: UserId,
    //signatures: Signatures,
    pub token: String,
}

#[derive(Debug, Deserialize)]
pub enum Membership {
    #[serde(rename="join")]
    Join,
    #[serde(rename="leave")]
    Leave,
    #[serde(rename="ban")]
    Ban,
    #[serde(rename="invite")]
    Invite,
    #[serde(rename="knock")]
    Knock,
}

#[derive(Debug, Deserialize)]
pub struct Unsigned {
    pub prev_sender: Option<String>,
    pub prev_content: Option<EventContent>,
    pub replaces_state: Option<String>,
    pub age: Option<f64>,
    pub redacted_because: Option<Box<Event>>,
    pub transaction_id: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct EventId(pub String);
#[derive(Debug, Deserialize, Hash, Eq, PartialEq)]
pub struct UserId(pub String);
#[derive(Debug, Deserialize)]
pub struct StateKey(pub String);
#[derive(Debug, Deserialize)]
pub struct RoomId(pub String);
#[derive(Debug, Deserialize)]
pub struct EventType(pub String);

impl ::std::fmt::Display for EventId {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl ::std::fmt::Display for UserId {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl ::std::fmt::Display for StateKey {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl ::std::fmt::Display for RoomId {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl ::std::fmt::Display for EventType {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self)
    }
}

