use std::collections::HashMap;

use ::events;

#[derive(Debug, Deserialize)]
pub struct SyncResponse {
    pub next_batch: String,
    pub rooms: Rooms,
    pub presence: Presence,
    pub account_data: AccountData,
}

#[derive(Debug, Deserialize)]
pub struct Rooms {
    pub leave: Option<HashMap<String, LeftRoom>>,
    pub join: Option<HashMap<String, JoinedRoom>>,
    pub invite: Option<HashMap<String, InvitedRoom>>,
}

#[derive(Debug, Deserialize)]
pub struct JoinedRoom {
    pub unread_notifications: UnreadNotificationCounts,
    pub timeline: Timeline,
    pub state: State,
    pub account_data: AccountData,
    pub ephemeral: Ephemeral,
}

#[derive(Debug, Deserialize)]
pub struct LeftRoom {
    pub timeline: Timeline,
    pub state: State,
}

#[derive(Debug, Deserialize)]
pub struct InvitedRoom {
    pub invite_state: InviteState,
}

#[derive(Debug, Deserialize)]
pub struct Presence {
    pub events: Vec<events::Event>,
}

#[derive(Debug, Deserialize)]
pub struct Timeline {
    pub limited: bool,
    pub prev_batch: String,
    pub events: Vec<events::Event>,
}

#[derive(Debug, Deserialize)]
pub struct State {
    pub events: Vec<events::Event>,
}

#[derive(Debug, Deserialize)]
pub struct AccountData {
    pub events: Vec<events::Event>,
}

#[derive(Debug, Deserialize)]
pub struct Ephemeral {
    pub events: Vec<events::Event>,
}

#[derive(Debug, Deserialize)]
pub struct InviteState {
    pub events: Vec<events::Event>,
}

#[derive(Debug, Deserialize)]
pub struct UnreadNotificationCounts {
    pub highlight_count: Option<f64>,
    pub notification_count: Option<f64>,
}

#[derive(Debug, Deserialize)]
pub struct RoomResponse {
    pub room_id: String,
    pub servers: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct LoginResponse {
    pub user_id: String,
    pub access_token: String,
    pub home_server: String,
    pub refresh_token: Option<String>,
    pub device_id: Option<String>, // See Server::login
}

#[derive(Debug, Deserialize)]
pub struct PublicRoomsResponse {
    start: String,
    end: String,
    chunk: Vec<PublicRoomsChunk>,
}

#[derive(Debug, Deserialize)]
pub struct PublicRoomsChunk {
    world_readable: bool,
    topic: Option<String>,
    num_joined_members: f64,
    avatar_url: Option<String>,
    room_id: String,
    guest_can_join: bool,
    aliases: Vec<String>,
    name: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct MessagesResponse {
    pub start: String,
    pub end: String,
    pub chunk: Vec<events::Event>,
}
